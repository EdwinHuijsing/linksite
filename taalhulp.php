<?php
/**********************/
/*    taalhulp.php    */
/*    Linksite        */
/*                    */
/**********************/
include 'config.php';
include 'header.php';
?>
    <title><?php echo TITLE;?> Taalhulp</title>
  </head>
<body>
<table class="border" border="<?php echo T_BORDER; ?>" height="100%" width="100%" cellpadding="<?php echo T_PAD; ?>" cellspacing="<?php echo T_SPAC; ?>">
  <tbody>
    <!-- bof menu-->
    <tr valign="top">
      <td class="bordermainmenu" rowSpan="2" colspan="1" width="<?php echo W_MENU; ?>"><?php include "menu/menu.php"?></td>
        <!-- eof menu -->
        <!-- bof header-->
      <td class="bordertopmenu" height="90">
          <B><FONT size="+2">Taalhulp</FONT></B>
          <div class="subtitle">Hulp voor vertalen van sites en software</div>
      </td>
    </tr>
    <!-- eof header-->
    <!-- bof main-->
    <tr valign="top">
      <td class="bordercontent"><?php include "content/taalhulp.php"?></td>
    </tr>
    <!-- eof main-->
  </tbody>
</table>
<? include 'footer.php';?>
</body>
</html>
