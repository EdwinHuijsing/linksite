<?php
/*******************/
/*    OS.php       */
/*    Linksite     */
/*                 */
/*******************/
include 'config.php';
include 'header.php';
?>
    <title><?php echo TITLE;?> OS</title>
  </head>
<body>
<table class="border" border="<?php echo T_BORDER; ?>" height="100%" width="100%" cellpadding="<?php echo T_PAD; ?>" cellspacing="<?php echo T_SPAC; ?>">
  <tbody>
   <!-- bof menu-->
    <tr valign="top">
      <td class="bordermainmenu" rowSpan="2" colspan="1" width="<?php echo W_MENU; ?>"><?php include "menu/menu.php"?></td>
      <!-- eof menu -->
      <!-- bof header-->
      <td class="bordertopmenu" height="90"><?php include "menu/OS.php"?></td>
    </tr>
      <!-- eof header-->
      <!-- bof main-->
    <tr valign="top">
      <td class="bordercontent">
         <?php
            $os = $_GET['os'];
            $ossub = $_GET['ossub'];
            if ($ossub == ''){
               Switch ($os) {
                  Case 'home':
                     include "content/OS/home.php";
                     Break;
                  Case 'haiku':
                     include "content/OS/haiku.php";
                     Break;
                  Case 'linux':
                     include "content/OS/home.php";
                     Break;
                  Default:
                     include "content/sorry.php";
               };
            } else {
               Switch ($ossub) {
                  Case 'mandriva':
                     include "content/OS/mandriva.php";
                     Break;
                  Case 'fedora':
                     include "content/sorry.php";
                     Break;
                  Case 'knoppix':
                     include "content/sorry.php";
                     Break;
                  Case 'debian':
                     include "content/sorry.php";
                     Break;
                  Case 'gentoo':
                     include "content/sorry.php";
                     Break;
                  Case 'pclinuxos':
                     include "content/sorry.php";
                     Break;
                  Default:
                  include "content/sorry.php";
                     Break;
               };
            }
         ?>
      </td>
    </tr>
<!-- eof main-->
  </tbody>
</table>
<? include 'footer.php';?>
</body>
</html>
