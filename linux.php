<?php
/*******************/
/*    linux.php    */
/*    Linksite     */
/*                 */
/*******************/
include 'config.php';
include 'header.php';
?>
    <title><?php echo TITLE;?> Linux</title>
  </head>
<body>
<table class="border" border="<?php echo T_BORDER; ?>" width="100%" cellpadding="<?php echo T_PAD; ?>" cellspacing="<?php echo T_SPAC; ?>">
  <tbody>
    <!-- bof menu-->
    <tr valign="top">
      <td class="bordermainmenu" rowSpan="2" colspan="1" width="<?php echo W_MENU; ?>"><?php include "menu/menu.php"?></td>
        <!-- eof menu -->
        <!-- bof header-->
      <td class="bordertopmenu" height="90">
        <?php include "menu/linux.php"?>
      </td>
    </tr>
    <!-- eof header-->
    <!-- bof main-->
    <tr valign="top">
      <td class="bordercontent">
            <?php
                $phphtml = $_GET['linux'];
                if ($phphtml == 'home'){include "content/linux/linux.php";
                  } elseif ($phphtml == 'exsound'){include "content/linux/exsoundcart.php";
                  } elseif ($phphtml == 'hebrew'){include "content/linux/hebrew.php";
                  } elseif ($phphtml == 'rpm'){include "content/linux/rpm.php";
                  } elseif ($phphtml == 'drivers'){include "content/linux/drivers.php";
                  } elseif ($phphtml == 'others'){include "content/linux/others.php";
                  } elseif ($phphtml == 'pclinuxos'){include "content/linux/pclinuxos.php";
                  } elseif ($phphtml == 'mandriva'){include "content/linux/mandriva.php";
                  } elseif ($phphtml == 'knoppix'){include "content/linux/knoppix.php";
                  } elseif ($phphtml == 'distro'){include "content/linux/distro.php";
                  } else {
                  include "content/sorry.php";
                };
            ?>
      </td>
    </tr>
    <!-- eof main-->
  </tbody>
</table>
<? include 'footer.php';?>
</body>
</html>
