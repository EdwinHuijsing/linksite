<?php
/*********************************/
/*    content/office/home.php    */
/*    Linksite                   */
/*                               */
/*********************************/
?>
<br>
Maak hierboven een keuze uit de diverse mogelijkheden!
<br><br>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.functionx.com/index.htm" target="_blank">Tutorials From  FunctionX</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">This site provides tutorials and links on various computer languages, programming environments, and libraries.</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.bcschools.net/staff/AccessHelp.htm" target="_blank">Microsoft Office Tutorial</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.kubrussel.ac.be/onderwijs/letterenwijs/geschiedenis/vakken/HA/" target="_blank">Onderwijs aan de K.U. Brussel</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Hier zijn veel linken te vinden voor MS Office</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.fontstuff.com/" target="_blank">Martin Green's Office Tips</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Office Tips. Tips and tutorials for Microsoft Excel Access, Word, FrontPage and VBA from trainer and consultant Martin Green.</td><!-- eof -->
    </tr>
  </tbody>
</table>
