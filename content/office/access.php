<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.databasedev.co.uk/table-of-contents.html" target="_blank">Database Solutions for Microsoft Access | Table Of Contents | databasedev.co.uk</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Database Solutions for Microsoft Access. Database design and implementation articles, tips, tricks, code samples, Access FAQ's and downloadable database examples. Use this site map to find exactly what you are looking for within the site.</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.microsoft.com/technet/scriptcenter/resources/officetips/default.mspx" target="_blank">Mircosoft TechNet</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Office Space: Adding New Records to and Modifying Existing Records in a Microsoft Access Database<br><br>Weekly column that offers tips and tricks for scripting Microsoft Office applications. </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.techonthenet.com/access/index.php" target="_blank">Access Topics</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Access is a unique tool released by Microsoft that provides both the functionality of a database and the programming capabilities to create end-user screens.</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.teacherclick.com/access2003/index.htm" target="_blank">Microsoft Access 2003 free tutorial</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.functionx.com/vbaccess/" target="_blank">VBA For MS Access</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://cisnet.baruch.cuny.edu/holowczak/classes/2200/access/access1.html" target="_blank">Microsoft Access Tutorial - Prof. Holowczak</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.bcschools.net/staff/AccessHelp.htm" target="_blank">Microsoft Access Tutorial</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.tekstenuitleg.net/artikelen/cursus_database_ontwerpen/1" target="_blank">Cursus database ontwerpen</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.pcmenu.info/pc_help/access/index.htm" target="_blank">Access tips en trucs</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.win.tue.nl/~sidorova/ogo/Access/%25index.html" target="_blank">Access 2003</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.gratiscursus.be/access_2003/index.htm" target="_blank">gratis cursus Access</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.access-programmers.co.uk/forums/forumdisplay.php?s=602e5fa478545878b38a4ec56f08e64b&amp;f=3" target="_blank">Microsoft Access Discussion - Access World Forums</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.helpmij.nl/forum/forumdisplay.php?f=3" target="_blank">HELP Mij</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Alles over het databaseprogramma van de Microsoft Office familie.</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.fontstuff.com/access/index.htm" target="_blank">Martin Green's Access Tips</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"></td><!-- eof -->
    </tr>
  </tbody>
</table>
<h3>Tips</h3>
Weeknummers like 01<br>
<br>
1). week2: IIf([weekfield]<10,"0" &amp ([weekfield]),[weekfield])
2). Its much easier to use format(weeknr,"00") and/or to use a function like CInt to convert the returned string to a real number, then if you sort it... It will be the right order....(not tested !!!)
