<?php
/**********************************/
/*    content/linux/hebrew.php    */
/*    Linksite                    */
/*                                */
/**********************************/
?>
<h3>keyboard</h3><br>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.iglu.org.il/faq/cache/56.html">IGLU&#39;s Frequently Asked Questions: Hebrew keyboard layout</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://imagic.weizmann.ac.il/~dov/Hebrew/hebrew.xkbmap">http://imagic.weizmann.ac.il/~dov/Hebrew/hebrew.xkbmap</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.starr.net/is/type/altnum.htm">Accent Marks and Diacriticals, Alt Number Combinations, alt num, alt key</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.uyip.org/keyboards.html">Hebrew Keyboards</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.zigzagworld.com/HKTutor/hten.htm">Hebrew Keyboard Tutor</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.qsm.co.il/NewHebrew/Key1452e.htm">Standard Hebrew Keyboard with Niqud</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.uyip.org/keyboards.html">Hebrew Keyboards</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.davka.com/cgi-bin/product.cgi?product=17">Hebrew Keyboard Stickers</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://people.musc.edu/~adelmaas/Adelmanian_keyboard/">Aaron’s proposed Dvoraklike Hebrew keyboard layout</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://culmus.sourceforge.net/main.html" target="_blank">Culmus</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
