<h3>us-122</h3><br>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.wlug.org.nz/TascamAudioInterface">Tascam Audio Interface - Waikato Linux Users Group</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">The Tascam US-122 (and apparently others) is a USB Audio/MIDI interface device which lets you plug things like guitars and condenser mics into your computer via a USB interface.  An ALSA driver exists for it (snd-usb-usx2y), but it did not work out of the box for me with Ubuntu.  I tried the following steps to get my hardware working (based largely on a post made in this thread: http://ubuntuforums.org/archive/index.php/t-21851.html).  These steps are probably ubuntu-specific.  I have followed the instructions given in the aforementioned ubuntuforums post almost to the letter, except that I compiled from source rather than using i386 RPM versions of packages (mostly because I&#39;m running a 64-bit system).  This seems to make a difference in terms of paths and such.</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://ubuntuforums.org/showthread.php?t=21851">M-Audio USB Quattro audio interface not working - Ubuntu Forums</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">M-Audio USB Quattro audio interface not working Hardware &amp; Laptops</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2">A HREF="http://ubuntuforums.org/showthread.php?t=30891&page=3">HOWTO: USB Audio device - Page 3 - Ubuntu Forums</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"><Page 3-HOWTO: USB Audio device Faqs, Howto, Tips &amp; Tricks<br></td><!-- eof -->
    </tr>
  </tbody>
</table>
<br>
<H3>external soundcart</H3><br>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://linuxagora.com/vbforum/showthread.php?t=337">Terratec Aurion MK II USB external USB soundcard</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Linux Agora Forums (Previous: Debian Questions &amp; Discussion)Terratec Aurion MK II USB external USB soundcard Hardware &amp; Drivers</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.penlug.org/twiki/bin/view/Main/LinuxHardwareInfoCreativeSoundblasterAudigy2">LinuxHardwareInfoCreativeSoundblasterAudigy2 &lt; Main &lt; TWiki</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.musicstorekoeln.de/nl/global/13_17_CUSBA_112_PCM0005959-000/0/0/0/detail/musicstore.html">Tascam US 144 Musicstore</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Tascam US-144 The US-122LÉs big brother! The US-144 also features high quality mic inputs (XLR with phantom power), 96kHz/24-bit recording and zer</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.alsa-project.org/alsa-doc/index.php?vendor=vendor-Tascam#matrix">Advanced Linux Sound Architecture - ALSA Soundcard Matrix</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.linuxsampler.org/screenshots.html">The Linux Sampler Project</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<br>
<h3>Editing software</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://audacity.sourceforge.net/help/faq?s=files&i=crash-recovery" target="_blank">Audacity: Audacity crashed! Can I recover any unsaved data?</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<br><br>
