<?php
/***********************************/
/*    content/linux/drivers.php    */
/*    Linksite                     */
/*                                 */
/***********************************/
?>
<h3>NVIDIA</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.nvidia.com/object/linux_display_archive.html" target="_blank">Linux Display Driver Archive</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.nvidia.com/object/linux_display_ia32_1.0-8762.html" target="_blank">Linux Display Driver - IA32</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://nouveau.freedesktop.org/wiki/" target="_blank">nouveau Wiki - FrontPage</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
