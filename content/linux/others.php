<?php
/**************************/
/*    linux/others.php    */
/*    Linksite            */
/*                        */
/**************************/
?>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="https://www.sharcnet.ca/my/tutorials/linux_vi" target="_blank">Linux and VI Editor Fundamentals</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://linux.bytesex.org/xawtv/">xawtv homepage</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://users.pandora.be/tux/linuxbook.html">Het LinuX/UniX console handboek - homepage</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://appdb.winehq.org/appview.php?versionId=469">Wine Application DB - Viewing App- Internet Explorer Version - 6.0</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.tatanka.com.br/ies4linux/">IEs 4 Linux - Internet Explorers for Linux » English Page</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.mandrivaclub.nl/site/index.php?showtopic=5090&hl=octet">Coud not find mime type application/octet-stream - MandrivaClub.NL</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://insitu.lri.fr/metisse/">Metisse</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="https://lg3d.dev.java.net/">lg3d: LG3D Core Project</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.seerofsouls.com/">SoS: SeerOfSouls</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://forum.nedlinux.nl/viewtopic.php?t=19954&highlight=computerclub" target="_blank">Het Nederlandstalige Linuxforum :: Bekijk onderwerp - Linuxcomputerclub</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.mandrivaclub.nl/site/index.php?showtopic=3572">Clamav - MandrivaClub.NL</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.livre.nl/">Livre - Open Source Software en Open Standaarden</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://prdownloads.sourceforge.net/wine/">Browsing Download Server: /wine/</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.tatanka.com.br/ies4linux/forum/viewforum.php?f=3" target="_blank">IEs 4 Linux Forum :: View Forum - English Forum</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.jdisoftware.co.uk/pages/epdf-download.php" target="_blank">Download extendedPDF </A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://linmodems.technion.ac.il/" target="_blank">A Linmodems support page</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://linux.byexamples.com/" target="_blank">Linux By Examples - We explain every GNU/Linux command line by examples</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">We explain every GNU/Linux command by examples</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://club.mandriva.com/xwiki/bin/KB/InstallerScreenshots" target="_blank">schermafdrukken maken van de Mandriva installatie</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<br>
<h3>Software</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.lyx.org/" target="_blank">LyX - The Document Processor</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.freesoftwaremagazine.com/articles/documents_with_lyx?page=0,2" target="_blank">Structured writing with LyX</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.freepops.org/en/" target="_blank">http://www.freepops.org/en/ </A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<h2>VI(m)</h2>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.linuxfocus.org/Nederlands/May2000/article153.shtml" target="_blank">De VI editor leren gebruiken</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.cs.colostate.edu/helpdocs/vi.html" target="_blank">Basic vi Commands</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://vimdoc.sourceforge.net/cgi-bin/vimfaq2html3.pl" target="_blank">Vim online doc: Vim FA</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<br>
<h3>Zoeken naar Software</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.softpedia.com/" target="_blank">Free downloads encyclopedia - Softpedia</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://freshmeat.net/" target="_blank">freshmeat.net: Welcome to freshmeat.net</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://linuxappfinder.com/" target="_blank">Linux App Finder | Helping find the Linux apps you need</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>

<h3>Humor</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.allesis4.com/" target="_blank">Alles is 4</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Als je het niet gelooft probeer maar eens uit! </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.klikhierniet.net/" target="_blank">Klik hier niet</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Dit kan niet duidelijker <b>Klik <u>niet</u> op deze link</b> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.something.com/" target="_blank">something</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Hiet gaat hier echt over "iets" </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.turnofftheinternet.com/" target="_blank">Shutdown the internet</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Problemen met je internet? Dit is de oplossing! </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://ars.userfriendly.org/cartoons/?id=20050815" target="_blank">HELPdesk</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">no comment </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.oneliners-and-proverbs.com/computer.html" target="_blank">Computer tags (onliners)</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.beigebinary.com/you-may-have-been-using-linux-too-much-when" target="_blank">You may have been using Linux too much when...</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.beginnersweb.nl/forum/" target="_blank">BeginersWeb Forum</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Bijna alleen maar nieuwe komers aan het woord, het "noob"gehalte ligt tegen de 97% aan. Je komt dus vaak bijna niet meer bij van het lachen. </td><!-- eof -->
    </tr>
  </tbody>
</table><br><br>
En nog even een paar uitspraken :

<ul>
  <li>In a world without walls and fences, we don't need windows and gates.</li>
  <li>Linux: be root || Windows: reboot</li>
  <li>You are about to exit windows, do you want to play another game?</li>
  <li>Windows:<br>"you're mouse has moved. Please restart the computer for the change to take effect"</li>
  <li>Is Windows a Virus?<br>No, Windows is not a virus. Here's what viruses do:<br>* They replicate quickly - okay, Windows does that.<br>* Viruses use up valuable system resources, slowing down the system as they do so - okay, Windows does that.<br>* Viruses will, from time to time, trash your hard disk - okay, Windows does that too.<br>* Viruses are usually carried, unknown to the user, along with valuable programs and systems. Sigh... Windows does that, too.<br>* Viruses will occasionally make the user suspect their system is too slow (see 2) and the user will buy new hardware. Yup, that's with Windows, too.<br>Until now it seems Windows is a virus but there are fundamental differences:Viruses are well supported by their authors, are running on most systems, their program code is fast, compact and efficient and they tend to become more sophisticated as they mature.<br>So Windows is not a virus.<br>It's a bug.</li>
  <li>"Computers are like airconditioners,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;they stop working if you open windows."</li>
  <li>To create a crash working with Linux you have to work at it, to create a crash with Windows you have to work on it.<br>(to break windows, you neet to work on it. to break linux, you need to work at it.)</li>
</ul>
<dt>&#8226; <a href="http://www.linuxquestions.org/linux/answers/Hardware/Clean_Hard_Drive_zero_fill" target="_blank"> Clean Hard Drive (zero fill)</a></dt>
<dd></dd>
<dt>&#8226; <a href="http://en.linuxreviews.org/HOWTO_Dump_audio_streams_from_a_video_file" target="_blank">HOWTO Dump audio streams from a video file</a></dt>
<dd></dd>
<dt>&#8226; <a href="http://www.livecdlist.com/" target="_blank">The LiveCD List</a></dt>
<dd></dd>
<dt>&#8226; <a href="http://wiki.apache.org/spamassassin/RuleUpdates" target="_blank">RuleUpdates</a></dt>
<dd></dd>

<br><br>
