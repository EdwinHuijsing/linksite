<?php
/******************************/
/*    content/taalhulp.php    */
/*    Linksite                */
/*                            */
/******************************/
?>
<h3>Vertalen</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://tools.search.yahoo.com/language/" target="_blank">Yahoo! Search Language Tools</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.google.com/language_tools?hl=en" target="_blank">Google! Language Tools</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://dictionary.reference.com/translate/text.html" target="_blank">Dictionary.com/Translator</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://freetranslation.paralink.com/remote.asp#" target="_blank">Free translation, ImTranslator.com</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<br>
<h3>*.po vertalen</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.gentoo.org/doc/en/cvs-tutorial.xml" target="_blank">Gentoo Linux Documentation -- Gentoo Linux CVS Tutorial</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://nl.wikipedia.org/wiki/ISO_4217" target="_blank">ISO 4217 - Wikipedia NL</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://developer.gnome.org/tools/cvs.html" target="_blank">Tools - CVS</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://www.urbandictionary.com/define.php?term=%s" target="_blank">Urban Dictionary</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><A HREF="http://cvs.debian.org/cgi-bin/viewcvs.cgi/webwml/dutch/po/?cvsroot=webwml#dirlist" target="_blank">webwml/dutch/po</A></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<h3>Overige</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.opentaal.org/" target="_blank">Welkom bij OpenTaal</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">Het project OpenTaal maakt vrije Nederlandstalige taalhulpbestanden voor gebruik in opensourceprojecten. In eerste instantie werken we aan spellingcontrole en woordafbreking. Ook denken we aan synoniemenlijsten en er is een begin gemaakt met grammaticacontrole.</td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.opentaal.org/download_spellingbestanden.php" target="_blank">OpenTaal: downloaden spellingbestanden</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>
<h3>Leer een andere taal</h3>
<table width="100%" border="0">
  <tbody>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://www.skwierzyna.net/learn_polish.htm" target="_blank">Learn Polish Online (skwierzyna.net)</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0">*** </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://apronus.com/learnpolish/index.htm" target="_blank">Learn Polish at Apronus.com</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://101languages.net/polish/" target="_blank">Learn Polish online for free!(101languages.net)</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://grzegorj.w.interia.pl/kurs/index.html" target="_blank">A course of the Polish language</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
    <tr>
      <td>&#8226;</td><!-- bof -->
      <td colspan="2"><a href="http://onestoppolish.com/" target="_blank">onestoppolish</a></td>
    </tr>
    <tr>
      <td colspan="2" height="0"></td>
      <td height="0"> </td><!-- eof -->
    </tr>
  </tbody>
</table>

<br><br>
