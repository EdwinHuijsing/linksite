<?php
/*******************/
/*    health.php   */
/*    Linksite     */
/*                 */
/*******************/
include 'config.php';
include 'header.php';
?>
        <title><?php echo TITLE;?>Office</title>
  </head>
<body>
<table class="border" border="<?php echo T_BORDER; ?>" width="100%" cellpadding="<?php echo T_PAD; ?>" cellspacing="<?php echo T_SPAC; ?>">
  <tbody>
    <!-- bof menu-->
    <tr valign="top">
      <td class="bordermainmenu" rowSpan="2" colspan="1" width="<?php echo W_MENU; ?>"><?php include "menu/menu.php"?></td>
        <!-- eof menu -->
        <!-- bof header-->
      <td class="bordertopmenu" height="90"><?php include "menu/office.php"?></td>
    </tr>
    <!-- eof header-->
    <!-- bof main-->
    <tr valign="top">
      <td class="bordercontent">
            <?php
                $office = $_GET['office'];
                if (empty($_GET['office'])) {include "content/office/home.php";
        } else {
          if ($office == 'home'){include "content/office/home.php";
                    } elseif ($office == 'excel'){include "content/office/excel.php";
                    } elseif ($office == 'access'){include "content/office/access.php";
                    } elseif ($office == 'word'){include "content/office/word.php";
                    } elseif ($office == 'other'){include "content/office/empty.php";
                    } else {
                    include "content/sorry.php";
                  };
        }
            ?>
      </td>
    </tr>
    <!-- eof main-->
  </tbody>
</table>
<? include 'footer.php';?>
</body>
</html>
