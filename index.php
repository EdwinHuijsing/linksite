<?php
/*******************/
/*    index.php    */
/*    Linksite     */
/*                 */
/*******************/
Include 'config.php';
Require 'header.php';
?>
        <title><?php echo TITLE;?></title>
    </head>
<body>
<table class="border" border="<?php echo T_BORDER; ?>" width="100%" cellpadding="<?php echo T_PAD; ?>" cellspacing="<?php echo T_SPAC; ?>" summary="">
  <tbody>
    <!-- bof menu-->
    <tr valign="top">
      <td class="bordermainmenu" rowSpan="2" colspan="1" width="<?php echo W_MENU; ?>"><?php include "menu/menu.php"?><a class="valid" href="http://validator.w3.org/check?uri=referer" target="_blank"><img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Transitional" height="31" width="88"></a></td>
        <!-- eof menu -->
        <!-- bof header-->
      <td class="bordertopmenu" height="90"><?php include "home.php"?></td>
    </tr>
    <!-- eof header-->
    <!-- bof main-->
    <tr valign="top">
      <td class="bordercontent"><?php include "choice.html"?></td>
    </tr>
<!-- eof main-->
  </tbody>
</table>
<? include 'footer.php';?>
</body>
</html>
